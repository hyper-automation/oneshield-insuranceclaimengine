package com.oneshield.repositories;

import com.oneshield.entities.NatureOfInjury;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NatureOfInjuryRepository extends JpaRepository<NatureOfInjury, Integer> {
}
