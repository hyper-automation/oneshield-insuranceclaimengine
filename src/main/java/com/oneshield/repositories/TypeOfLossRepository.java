package com.oneshield.repositories;

import com.oneshield.entities.TypeOfLoss;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeOfLossRepository extends JpaRepository<TypeOfLoss, Integer> {
}
