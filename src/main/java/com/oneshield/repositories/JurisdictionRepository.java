package com.oneshield.repositories;

import com.oneshield.entities.Jurisdiction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JurisdictionRepository extends JpaRepository<Jurisdiction, Integer> {
}
