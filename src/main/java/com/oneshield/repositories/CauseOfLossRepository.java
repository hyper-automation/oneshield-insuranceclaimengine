package com.oneshield.repositories;

import com.oneshield.entities.CauseOfLoss;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CauseOfLossRepository extends JpaRepository<CauseOfLoss, Integer> {
}
