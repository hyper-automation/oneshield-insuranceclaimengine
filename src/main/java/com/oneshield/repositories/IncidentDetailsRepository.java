package com.oneshield.repositories;

import com.oneshield.entities.IncidentDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IncidentDetailsRepository extends JpaRepository<IncidentDetails, Integer> {

    Optional<IncidentDetails> findByOneshiledIncident(String oneshiledIncident);
}
