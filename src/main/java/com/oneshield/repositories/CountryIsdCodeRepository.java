package com.oneshield.repositories;

import com.oneshield.entities.CountryIsdCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryIsdCodeRepository extends JpaRepository<CountryIsdCode, Integer> {
}
