package com.oneshield.repositories;

import com.oneshield.entities.CauseOfInjury;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CauseOfInjuryRepository extends JpaRepository<CauseOfInjury, Integer> {
}
