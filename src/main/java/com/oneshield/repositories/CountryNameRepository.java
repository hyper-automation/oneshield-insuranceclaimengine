package com.oneshield.repositories;

import com.oneshield.entities.CountryName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryNameRepository extends JpaRepository<CountryName, Integer> {
}
