package com.oneshield.repositories;

import com.oneshield.entities.OcAssetType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OcAssetTypeRepository extends JpaRepository<OcAssetType, Integer> {
}
