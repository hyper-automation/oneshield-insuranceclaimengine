package com.oneshield.repositories;

import com.oneshield.entities.DocumentDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentDetailsRepotory extends JpaRepository<DocumentDetails, Integer> {
}
