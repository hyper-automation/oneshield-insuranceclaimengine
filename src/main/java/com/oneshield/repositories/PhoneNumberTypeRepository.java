package com.oneshield.repositories;

import com.oneshield.entities.PhoneNumberType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhoneNumberTypeRepository extends JpaRepository<PhoneNumberType, Integer> {
}
