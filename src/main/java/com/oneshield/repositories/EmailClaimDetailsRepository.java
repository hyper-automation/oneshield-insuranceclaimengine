package com.oneshield.repositories;

import com.oneshield.entities.EmailClaimDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmailClaimDetailsRepository extends JpaRepository<EmailClaimDetails, Integer> {
}
