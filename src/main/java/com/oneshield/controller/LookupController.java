package com.oneshield.controller;

import com.oneshield.entities.*;
import com.oneshield.service.LookupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class LookupController {

    @Autowired
    LookupService service;

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/CountryIsdCode", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CountryIsdCode> getCountryIsdCode() {
        return service.getCountryIsdCode();
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/PhoneNumberType", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PhoneNumberType> getPhoneNumberType() {
        return service.getPhoneNumberType();
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/EmailType", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<EmailType> getEmailType() {
        return service.getEmailType();
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/Jurisdiction", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Jurisdiction> getJurisdiction() {
        return service.getJurisdiction();
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/CountryName", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CountryName> getCountryName() {
        return service.getCountryName();
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/IncidentType", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<IncidentType> getIncidentType() {
        return service.getIncidentType();
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/CauseOfLoss", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CauseOfLoss> getCauseOfLoss() {
        return service.getCauseOfLoss();
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/TypeOfLoss", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TypeOfLoss> getTypeOfLoss() {
        return service.getTypeOfLoss();
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/Gender", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Gender> getGender() {
        return service.getGender();
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/Occupation", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Occupation> getOccupation() {
        return service.getOccupation();
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/NatureOfInjury", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NatureOfInjury> getNatureOfInjury() {
        return service.getNatureOfInjury();
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/CauseOfInjury", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CauseOfInjury> getCauseOfInjury() {
        return service.getCauseOfInjury();
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/BodyPart", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BodyPart> getBodyPart() {
        return service.getBodyPart();
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/Role", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Role> getRole() {
        return service.getRole();
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/OcAssetType", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OcAssetType> getOcAssetType() {
        return service.getOcAssetType();
    }
}
