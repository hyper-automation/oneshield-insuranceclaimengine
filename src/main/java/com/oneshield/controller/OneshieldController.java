package com.oneshield.controller;

import com.oneshield.service.OneShieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class OneshieldController {

    @Autowired
    OneShieldService service;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/processFnol/{source}")
    public String processFnol(@PathVariable String source,@RequestBody String jsonString) {
       return service.processFnol(source,jsonString);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/incidentStatus/{incidentNumber}")
    public String incidentStatus(@PathVariable String incidentNumber) {
        return service.incidentStatus(incidentNumber);
    }
}
