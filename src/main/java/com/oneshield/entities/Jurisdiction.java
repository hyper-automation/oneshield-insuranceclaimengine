package com.oneshield.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "jurisdiction")
@Entity
@Getter
@Setter
@Data
public class Jurisdiction implements Serializable {
    @Id
    @Column
    private Integer jurdistictionID;
    @Column
    private String jurdistiction;
    @Column
    private String jurdistictionCode;
}
