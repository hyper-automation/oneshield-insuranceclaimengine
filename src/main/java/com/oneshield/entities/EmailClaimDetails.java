package com.oneshield.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Table(name = "emailClaimDetails")
@Entity
@Getter
@Setter
@Data
public class EmailClaimDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer emailClaimID;
    @Column
    private String emailBody;
    @Column
    private String subject;
    @Column
    private String FromEmailID;
    @Column
    private String ccEmailIds;
    @Column
    private Boolean hasAttachments;
    @Column
    private String documentIDs;
    @Column
    private LocalDateTime recievedOn;
    @Column
    private String businessStatus;
    @Column
    private String status;
    @Column
    private String modifiedBy;
    @Column
    private LocalDateTime modifiedOn;

}
