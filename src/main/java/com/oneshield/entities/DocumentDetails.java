package com.oneshield.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Table(name = "documentDetails")
@Getter
@Setter
@Entity
public class DocumentDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer documentID;
    @Column
    private String documentTitle;
    @Column
    private String documentURL;
    @Column
    private String documentFormat;
    @Column
    private String status;
    @Column
    private String modifiedBy;
    @Column
    private LocalDateTime modifiedOn;

}
