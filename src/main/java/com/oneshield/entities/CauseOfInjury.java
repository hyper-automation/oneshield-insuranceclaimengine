package com.oneshield.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "causeOfInjury")
@Getter
@Setter
@Data
@Entity
public class CauseOfInjury implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer causeinjuryID;
    @Column
    private String causeinjury;

}
