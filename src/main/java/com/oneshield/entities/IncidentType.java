package com.oneshield.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "incidentType")
@Entity
@Getter
@Setter
@Data
public class IncidentType implements Serializable {
    @Id
    @Column
    private Integer incidentID;
    @Column
    private String incidentType;
}
