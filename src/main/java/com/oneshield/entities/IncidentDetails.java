package com.oneshield.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Table(name = "incidentDetails")
@Entity
@Data
@Getter
@Setter
public class IncidentDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer incidentID;
    @Column
    private String oneshiledIncident;
    @Column
    private Integer emailClaimID;
    @Column
    private String exctractedBody;
    @Column
    private String requestedBody;
    @Column
    private String responseBody;
    @Column
    private String status;
    @Column
    private String modifiedBy;
    @Column
    private String createdBy;
    @Column
    private LocalDateTime modifiedOn;
    @Column
    private String incidentStatus;
    @Column
    private String source;
}
