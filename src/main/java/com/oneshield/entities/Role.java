package com.oneshield.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "role")
@Getter
@Setter
@Data
public class Role implements Serializable {
    @Id
    @Column
    private Integer roleID;
    @Column
    private String role;
}
