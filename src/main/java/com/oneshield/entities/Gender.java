package com.oneshield.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "gender")
@Entity
@Data
@Getter
@Setter
public class Gender implements Serializable {
    @Id
    @Column
    private Integer genderID;
    @Column
    private String gender;
}
