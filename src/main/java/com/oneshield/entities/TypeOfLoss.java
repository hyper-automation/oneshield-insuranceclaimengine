package com.oneshield.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "typeOfLoss")
@Entity
@Data
@Getter
@Setter
public class TypeOfLoss implements Serializable {
    @Id
    @Column
    private Integer id;
    @Column
    private String typeofLoss;
    @Column
    private String active;
    @Column
    private String description;
}
