package com.oneshield.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "emailType")
@Data
@Getter
@Setter
public class EmailType implements Serializable {
    @Id
    @Column
    private Integer emailID;
    @Column
    private String emailDesription;
}
