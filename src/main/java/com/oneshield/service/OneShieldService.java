package com.oneshield.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OneShieldService {

    @Autowired
    OneShieldServiceHelper serviceHelper;

    public String processFnol(String source,String jsonString) {
        return serviceHelper.processFnol(source,jsonString);
    }

    public String incidentStatus(String incidentNumber) {
        return serviceHelper.incidentStatus(incidentNumber);
    }

}
