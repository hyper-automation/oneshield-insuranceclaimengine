package com.oneshield.service;

import com.oneshield.entities.IncidentDetails;
import com.oneshield.service.module.IncidentDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPMessage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

@Component
@Slf4j
public class OneShieldServiceHelper {

    @Autowired
    IncidentDetailsService incidentDetailsService;

    public String incidentStatus(String incidentNumber){
        IncidentDetails incidentDetails=new IncidentDetails();
        incidentDetailsService.get(incidentNumber,incidentDetails);
        log.info("incidentDetails :"+incidentDetails.toString());
        log.info("Status :"+incidentDetails.getIncidentStatus());
        return incidentDetails.getIncidentStatus();
    }

    public String processFnol(String source,String jsonString) {
        if(source.equals("Web")){
            JSONObject json=new JSONObject(jsonString);
            String xml = XML.toString(json);
            String soapRequest = formRequestxml(xml);
            String soapResp = callSoapService(soapRequest);
            return soapResp;
        }
        IncidentDetails incidentDetails = new IncidentDetails();
        incidentDetails.setExctractedBody(jsonString);
        JSONObject json = new JSONObject(jsonString);
        String email = fromEmail(json);
        String xml = XML.toString(json);
        incidentDetails.setRequestedBody(xml);
        String soapRequest = formRequestxml(xml);
        String soapResp = callSoapService(soapRequest);
        incidentDetails.setResponseBody(soapResp);
        String incidentNumber = processResponse(soapResp,incidentDetails);
        incidentDetails.setOneshiledIncident(incidentNumber);
        incidentDetails.setModifiedBy(email);
        incidentDetails.setCreatedBy(email);
        incidentDetails.setSource(source);
        incidentDetailsService.save(incidentDetails);
        return incidentNumber;
    }

    private String processResponse(String soapResp,IncidentDetails incidentDetails) {
        try {
            MessageFactory factory = MessageFactory.newInstance();
            SOAPMessage message = factory.createMessage(
                    new MimeHeaders(),
                    new ByteArrayInputStream(soapResp.getBytes(Charset
                            .forName("UTF-8"))));
            SOAPBody body = message.getSOAPBody();
            Node executeWorkflowResponseNode = body.getChildNodes().item(0);
            Node returnNode = executeWorkflowResponseNode.getChildNodes().item(0);
            NodeList nodeList = returnNode.getChildNodes();
            String content = "";
            String status="";
            for (int k = 0; k < nodeList.getLength(); k++) {
                if (nodeList.item(k).getNodeName()
                        .equalsIgnoreCase("payload")) {
                    content = nodeList.item(k).getTextContent();
                }
                if (nodeList.item(k).getNodeName()
                        .equalsIgnoreCase("contextObjectStatus")) {
                    status = nodeList.item(k).getChildNodes().item(1).getTextContent();
                }

            }
            if(!(content.isEmpty()||content.equals(""))) {
                JSONObject xmlJSONObj = XML.toJSONObject(content);
                log.info("payload :" + xmlJSONObj.toString());
                String incidentNumber = xmlJSONObj.getJSONObject("Incident").get("IncidentNumber").toString();
                log.info("IncidentNumber :" + incidentNumber);
                log.info("Status :" + status);
                incidentDetails.setOneshiledIncident(incidentNumber);
                incidentDetails.setIncidentStatus(status);
                return incidentNumber;
            }
            incidentDetails.setIncidentStatus("FNOL not reported");
            return "FNOL is not created";

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return e.getMessage();
        }
    }

    private String callSoapService(String soapRequest) {
        try {
            String url = "https://basedev3.oneshield.com/DisWeb/services/WorkflowService/executeWorkflow"; // replace your URL here
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // change these values as per soapui request on top left of request, click on RAW, you will find all the headers
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/xml; charset=utf-8");
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(soapRequest);
            wr.flush();
            wr.close();
            String responseStatus = con.getResponseMessage();
            log.info(responseStatus);
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            String finalvalue = response.toString();
            return finalvalue;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return e.getMessage();
        }
    }

    private String formRequestxml(String xml) {
        String requestxml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wor=\"http://workflow.service.oneshield.com/\" xmlns:xsd=\"http://io.service.oneshield.com/xsd\" xmlns:xsd1=\"http://io.common.integration.oneshield.com/xsd\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "          <wor:executeWorkflow>\n" +
                "         <wor:request>\n" +
                "            <xsd:payload><![CDATA[<?xml version=\"1.0\" ?>" +
                xml +
                "]]></xsd:payload>\n" +
                "            <xsd:operationLabel>registerFNOL</xsd:operationLabel>\n" +
                "            <exchangeId>1218</exchangeId>\n" +
                "            <partnerId>0</partnerId>\n" +
                "            <password>Oneshield1</password>\n" +
                "            <serviceOpInvocationId>111111111</serviceOpInvocationId>\n" +
                "            <systemId>OS</systemId>\n" +
                "            <userName>auto_adj</userName>\n" +
                "            <processingDirectives>\n" +
                "               <directive>\n" +
                "                  <name>CREATE_SESSION</name>\n" +
                "                  <value>true</value>\n" +
                "               </directive>\n" +
                "            </processingDirectives>\n" +
                "         </wor:request>\n" +
                "      </wor:executeWorkflow>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
        return requestxml;
    }

    private String fromEmail(JSONObject json) {
        return json.getJSONObject("IncidentReport").getJSONObject("ClaimantDetails").getJSONObject("EntityEmail").getString("Email");
    }
}
