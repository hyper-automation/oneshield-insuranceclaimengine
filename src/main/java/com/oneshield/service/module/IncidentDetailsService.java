package com.oneshield.service.module;

import com.oneshield.entities.IncidentDetails;

public interface IncidentDetailsService {
    public void save(IncidentDetails incidentDetails);
    public void get(String incidentNumber,IncidentDetails incidentDetails);
}
