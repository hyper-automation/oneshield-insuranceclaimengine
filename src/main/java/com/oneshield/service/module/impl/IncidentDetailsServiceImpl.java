package com.oneshield.service.module.impl;

import com.oneshield.entities.IncidentDetails;
import com.oneshield.repositories.IncidentDetailsRepository;
import com.oneshield.service.module.IncidentDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class IncidentDetailsServiceImpl implements IncidentDetailsService {

    @Autowired
    IncidentDetailsRepository incidentDetailsRepository;

    @Override
    public void save(IncidentDetails incidentDetails) {
        incidentDetails.setModifiedOn(LocalDateTime.now());
        incidentDetails.setStatus("Active");
        incidentDetailsRepository.save(incidentDetails);
    }

    @Override
    public void get(String incidentNumber,IncidentDetails incidentDetails) {
        IncidentDetails incidentDetailsDb=incidentDetailsRepository.findByOneshiledIncident(incidentNumber).get();
        incidentDetails.setIncidentID(incidentDetailsDb.getIncidentID());
        incidentDetails.setIncidentStatus(incidentDetailsDb.getIncidentStatus());
        incidentDetails.setOneshiledIncident(incidentDetailsDb.getOneshiledIncident());
        incidentDetails.setEmailClaimID(incidentDetailsDb.getEmailClaimID());
        incidentDetails.setModifiedOn(incidentDetailsDb.getModifiedOn());
        incidentDetails.setModifiedBy(incidentDetailsDb.getModifiedBy());
        incidentDetails.setStatus(incidentDetailsDb.getStatus());
        incidentDetails.setExctractedBody(incidentDetailsDb.getExctractedBody());
        incidentDetails.setRequestedBody(incidentDetailsDb.getResponseBody());
        incidentDetails.setResponseBody(incidentDetailsDb.getResponseBody());
    }

}
