package com.oneshield.service;

import com.oneshield.entities.*;
import com.oneshield.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LookupService {
    @Autowired
    CountryIsdCodeRepository countryIsdCodeRepository;
    @Autowired
    PhoneNumberTypeRepository phoneNumberTypeRepository;
    @Autowired
    EmailTypeRepository emailTypeRepository;
    @Autowired
    JurisdictionRepository jurisdictionRepository;
    @Autowired
    CountryNameRepository countryNameRepository;
    @Autowired
    IncidentTypeRepostory incidentTypeRepostory;
    @Autowired
    CauseOfLossRepository causeOfLossRepository;
    @Autowired
    TypeOfLossRepository typeOfLossRepository;
    @Autowired
    GenderRepository genderRepository;
    @Autowired
    OccupationRepository occupationRepository;
    @Autowired
    NatureOfInjuryRepository natureOfInjuryRepository;
    @Autowired
    CauseOfInjuryRepository causeOfInjuryRepository;
    @Autowired
    BodyPartRepository bodyPartRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    OcAssetTypeRepository ocAssetTypeRepository;

    public List<CountryIsdCode> getCountryIsdCode() {
        try {
            return countryIsdCodeRepository.findAll();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<PhoneNumberType> getPhoneNumberType() {
        try {
            return phoneNumberTypeRepository.findAll();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<EmailType> getEmailType() {
        try {
            return emailTypeRepository.findAll();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<Jurisdiction> getJurisdiction() {
        try {
            return jurisdictionRepository.findAll();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<CountryName> getCountryName() {
        try {
            return countryNameRepository.findAll();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<IncidentType> getIncidentType() {
        try {
            return incidentTypeRepostory.findAll();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<CauseOfLoss> getCauseOfLoss() {
        try {
            return causeOfLossRepository.findAll();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<TypeOfLoss> getTypeOfLoss() {
        try {
            return typeOfLossRepository.findAll();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<Gender> getGender() {
        try {
            return genderRepository.findAll();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<Occupation> getOccupation() {
        try {
            return occupationRepository.findAll();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<NatureOfInjury> getNatureOfInjury() {
        try {
            return natureOfInjuryRepository.findAll();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<CauseOfInjury> getCauseOfInjury() {
        try {
            return causeOfInjuryRepository.findAll();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<BodyPart> getBodyPart() {
        try {
            return bodyPartRepository.findAll();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<Role> getRole() {
        try {
            return roleRepository.findAll();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<OcAssetType> getOcAssetType() {
        try {
            return ocAssetTypeRepository.findAll();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }


}
